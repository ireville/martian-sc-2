package martian;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ConservativeMartian<T> extends Martian<T> {
    private final T value;
    private final Martian<T> parent;
    private final ArrayList<Martian<T>> children;

    ConservativeMartian(T value, Martian<T> parent, ArrayList<Martian<T>> children) {
        this.value = value;
        this.parent = parent;
        this.children = children;
    }

    public ConservativeMartian(InnovatorMartian<T> innovator) {
        ConservativeMartian<T> cons = getRightChild(innovator);
        this.value = cons.value;
        this.parent = cons.parent;
        this.children = cons.children;
    }

    @Override
    public T getValue() {
        return this.value;
    }

    @Override
    public Martian<T> getParent() {
        return this.parent;
    }

    /**
     * Возвращает список всех детей марсианина.
     *
     * @return Список всех детей марсианина.
     */
    @Override
    public List<Martian<T>> getChildren() {
        return Collections.unmodifiableList(children);
    }

    /**
     * Получает порядковый номер марсианина в списке детей его родителя.
     *
     * @param inn Марсианин, для которого считается номер.
     * @return Номер.
     */
    int getChildIndex(InnovatorMartian<T> inn) {
        InnovatorMartian<T> parent = (InnovatorMartian<T>) inn.getParent();
        if (parent == null) {
            return -1;
        }

        for (int i = 0; i < parent.getChildren().size(); i++) {
            if (parent.getChildren().get(i).equals(inn)) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Возвращает нужного консерватора (близнеца) из дерева консерваторов, построенного по инноватору.
     *
     * @param inn Инноватор-близнец.
     * @return Консерватор.
     */
    ConservativeMartian<T> getRightChild(InnovatorMartian<T> inn) {
        if (inn.getParent() == null) {
            return buildConservativeFromInnovator(inn, null);
        }

        int childIndex = getChildIndex(inn);
        ConservativeMartian<T> parent = getRightChild((InnovatorMartian<T>) inn.getParent());

        return (ConservativeMartian<T>) parent.getChildren().get(childIndex);
    }

    /**
     * Рекурсивно строит семейное дерево консерваторов по переданному инноватору.
     *
     * @param innTwin Инноватор, по которому строится дерево.
     * @param parent  Родитель создаваемого консерватора.
     * @return Корень построенного дерева.
     */
    ConservativeMartian<T> buildConservativeFromInnovator(InnovatorMartian<T> innTwin,
                                                          ConservativeMartian<T> parent) {
        ArrayList<Martian<T>> childs = new ArrayList<>();
        ConservativeMartian<T> current = new ConservativeMartian<>(innTwin.getValue(), parent, childs);

        for (Martian<T> innChild : innTwin.getChildren()) {
            childs.add((buildConservativeFromInnovator((InnovatorMartian<T>) innChild, current)));
        }

        return current;
    }
}
