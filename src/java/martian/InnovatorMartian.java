package martian;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class InnovatorMartian<T> extends Martian<T> {
    private T value;
    private Martian<T> parent;
    private ArrayList<Martian<T>> children;

    public InnovatorMartian(T value, Martian<T> parent, ArrayList<Martian<T>> children) {
        this.value = value;
        this.children = new ArrayList<>();

        boolean correctParent = checkParentForThis(parent);
        if (correctParent) {
            this.parent = parent;
        } else {
            throw new IllegalArgumentException("Incorrect parent. ");
        }

        boolean correctChildren = setChildren(children);
        if (!correctChildren) {
            throw new IllegalArgumentException("Incorrect children. ");
        }
    }

    @Override
    public T getValue() {
        return value;
    }

    /**
     * Задаёт новый генетический код марсианина.
     *
     * @param value Новый генетический код.
     */
    public void setValue(T value) {
        this.value = value;
    }

    @Override
    public Martian<T> getParent() {
        return parent;
    }

    /**
     * Возвращает список всех детей марсианина.
     *
     * @return Список всех детей марсианина.
     */
    @Override
    public List<Martian<T>> getChildren() {
        return Collections.unmodifiableList(children);
    }

    /**
     * Задаёт нового родителя марсианина.
     *
     * @param newParent Новый родитель.
     * @return Истина, если новый родитель мог стать и стал родителем, ложь - иначе.
     */
    public boolean setParent(Martian<T> newParent) {
        if (newParent == null) {
            if (this.parent != null) {
                ((InnovatorMartian<T>) this.parent).removeChild(this);
            }

            this.parent = null;
            return true;
        }

        if (!checkParentForThis(newParent)) {
            return false;
        }

        // У нашего старого родителя удаляем себя.
        if (this.parent != null) {
            ((InnovatorMartian<T>) this.parent).deleteChild(this);
        }

        // Себе ставим родителя.
        this.parent = newParent;

        // Добавляем себя к детям родителя.
        ((InnovatorMartian<T>) newParent).children.add(this);

        return true;
    }

    /**
     * Задаёт новых детей марсианина.
     *
     * @param newChildren Новые дети.
     * @return Истина, если новые дети могли стать и стали детьми, ложь - иначе.
     */
    public boolean setChildren(Collection<Martian<T>> newChildren) {
        if (newChildren == null) {
            newChildren = new ArrayList<>();
        }

        // Проверяем, подходят ли дети.
        for (Martian<T> child : newChildren) {
            if (!this.checkChildForThis(child)) {
                return false;
            }
        }

        newChildren = new ArrayList<>(newChildren);

        if (!children.isEmpty()) {
            // У своих детей удаляем родителя.
            InnovatorMartian<T> oldChild;
            ArrayList<Martian<T>> oldChildren = new ArrayList<>(children);
            for (int i = 0; i < oldChildren.size(); i++) {
                oldChild = (InnovatorMartian<T>) oldChildren.get(i);
                oldChild.setParent(null);
            }

            children.clear();
        }

        for (Martian<T> child : newChildren) {
            addChild(child);
        }

        return true;
    }

    /**
     * Добавляет нового ребёнка родителю.
     *
     * @param child Новый ребёнок.
     * @return Истина, если ребёнок был корректно добавлен, ложь - иначе.
     */
    public boolean addChild(Martian<T> child) {
        if (!this.checkChildForThis(child)) {
            return false;
        }

        if (child.getParent() != null) {
            // У старого родителя удаляем ребенка.
            ((InnovatorMartian<T>) child.getParent()).deleteChild(child);
        }

        // У нового ребенка меняем родителя.
        if (!((InnovatorMartian<T>) child).setParent(this)) {
            return false;
        }

        return true;
    }

    /**
     * Проверяет, может ли марсианин стать ребёнком данному (нет ли противоречий в дереве).
     *
     * @param child Потенциальный ребёнок.
     * @return Истина, если может, ложь - иначе.
     */
    boolean checkChildForThis(Martian<T> child) {
        // this becomes a parent

        // Проверяем тип.
        if (!(child instanceof InnovatorMartian)) {
            return false;
        }
        // Проверить, не является ли родитель ребёнком или его потомком.
        if (this.equals(child) || this.isDescendantOf(child)) {
            return false;
        }

        return true;
    }

    /**
     * Проверяет, может ли марсианин стать родителем данному (нет ли противоречий в дереве).
     *
     * @param newParent Потенциальный родитель.
     * @return Истина, если может, ложь - иначе.
     */
    boolean checkParentForThis(Martian<T> newParent) {
        // this becomes a child

        if (newParent == null) {
            return true;
        }

        // Проверяем тип.
        if (!(newParent instanceof InnovatorMartian)) {
            return false;
        }

        if (this.equals(newParent) || (newParent.isDescendantOf(this))) {
            return false;
        }

        return true;
    }

    /**
     * Удаляет ребёнка у родителя, осуществляя необходимые изменения в дереве.
     *
     * @param child Ребёнок.
     * @return Истина, если у данного родителя был такой ребёнок и он был удалён, ложь - иначе
     */
    public boolean deleteChild(Martian<T> child) {
        if (!(child instanceof InnovatorMartian)) {
            return false;
        }

        // Удаляем родителя у этого ребенка.
        if (children.contains(child)) {
            ((InnovatorMartian<T>) child).setParent(null);
            children.remove(child);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Удаляет ребёнка у из детей родителя.
     *
     * @param child Ребёнок для удаления.
     */
    private void removeChild(Martian<T> child) {
        children.remove(child);
    }
}
