package martian;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTreeTest {
    static InnovatorMartian<Double> c1 = new InnovatorMartian<>(1.111, null, new ArrayList<>());
    static InnovatorMartian<Double> c2 = new InnovatorMartian<>(2.222, c1, new ArrayList<>());
    static InnovatorMartian<Double> c3 = new InnovatorMartian<>(10230.12392, c1, new ArrayList<>());
    static InnovatorMartian<Double> c4 = new InnovatorMartian<>(18.2419281, c1, new ArrayList<>());
    static InnovatorMartian<Double> c5 = new InnovatorMartian<>(28287348.2, c3, new ArrayList<>());
    static InnovatorMartian<Double> c6 = new InnovatorMartian<>(-182.90, c3, new ArrayList<>());
    static InnovatorMartian<Double> c7 = new InnovatorMartian<>(-7.0, c4, new ArrayList<>());
    static InnovatorMartian<Double> c8 = new InnovatorMartian<>(-918291.348, c5, new ArrayList<>());


    @BeforeEach
    void setUp() {
        ArrayList<Martian<Double>> childs = new ArrayList<>();
        childs.add(c8);
        c5 = new InnovatorMartian<>(28287348.2, null, childs);

        childs.clear();
        childs.add(c7);
        c4 = new InnovatorMartian<>(18.2419281, null, childs);

        childs.clear();
        childs.add(c5);
        childs.add(c6);
        c3 = new InnovatorMartian<>(10230.12392, null, childs);

        // OK - Должны перекопировать из коллекции, иначе плохо будет. -OK
        childs.clear();
        childs.add(c2);
        childs.add(c3);
        childs.add(c4);
        c1 = new InnovatorMartian<>(1.111, null, childs);

        c8.setParent(c5);
        c7.setParent(c4);
        c5.setParent(c3);
        c6.setParent(c3);
        c2.setParent(c1);
        c3.setParent(c1);
        c4.setParent(c1);
    }

    @Test
    void getRoot() {
        FamilyTree tree = new FamilyTree(c3);
        assertEquals(c1.getValue(), ((Martian) tree.getRoot()).getValue());

        tree = new FamilyTree(c2);
        assertEquals(c1.getValue(), ((Martian) tree.getRoot()).getValue());

        tree = new FamilyTree(c4);
        assertEquals(c1.getValue(), ((Martian) tree.getRoot()).getValue());

        tree = new FamilyTree(c8);
        assertEquals(c1.getValue(), ((Martian) tree.getRoot()).getValue());

        ConservativeMartian<Double> cons = new ConservativeMartian<>(c4);

        tree = new FamilyTree(cons);
        assertEquals(c1.getValue(), ((Martian) tree.getRoot()).getValue());

        tree = new FamilyTree(cons.getChildren().get(0));
        assertEquals(c1.getValue(), ((Martian) tree.getRoot()).getValue());

        tree = new FamilyTree("InnovatorMartian(Double:1.111)" + System.lineSeparator() +
                "    InnovatorMartian(Double:2.222)" + System.lineSeparator() +
                "    InnovatorMartian(Double:10230.12392)" + System.lineSeparator() +
                "        InnovatorMartian(Double:2.82873482E7)" + System.lineSeparator() +
                "            InnovatorMartian(Double:-918291.348)" + System.lineSeparator() +
                "        InnovatorMartian(Double:-182.9)" + System.lineSeparator() +
                "    InnovatorMartian(Double:18.2419281)" + System.lineSeparator() +
                "        InnovatorMartian(Double:-7.0)" + System.lineSeparator());
        assertEquals(c1.getValue(), ((Martian<?>) tree.getRoot()).getValue());
        assertEquals(c3.getValue(), ((Martian<?>) tree.getRoot()).getChildren().get(1).getValue());

        tree = new FamilyTree("ConservativeMartian(Double:1.111)" + System.lineSeparator() +
                "    ConservativeMartian(Double:2.222)" + System.lineSeparator() +
                "    ConservativeMartian(Double:10230.12392)" + System.lineSeparator() +
                "        ConservativeMartian(Double:2.82873482E7)" + System.lineSeparator() +
                "            ConservativeMartian(Double:-918291.348)" + System.lineSeparator() +
                "        ConservativeMartian(Double:-182.9)" + System.lineSeparator() +
                "    ConservativeMartian(Double:18.2419281)" + System.lineSeparator() +
                "        ConservativeMartian(Double:-7.0)" + System.lineSeparator());

        assertTrue(((ConservativeMartian<Double>) tree.getRoot()) instanceof ConservativeMartian);

        InnovatorMartian<String> mart = null;
        tree = new FamilyTree(mart);
        assertEquals(null, tree.getRoot());
        assertEquals("No family tree for null martian. ", tree.getFamilyReport());
    }

    @Test
    void getFamilyReport() {
        FamilyTree tree = new FamilyTree(c2);
        assertEquals("InnovatorMartian(Double:1.111)" + System.lineSeparator() +
                "    InnovatorMartian(Double:2.222)" + System.lineSeparator() +
                "    InnovatorMartian(Double:10230.12392)" + System.lineSeparator() +
                "        InnovatorMartian(Double:2.82873482E7)" + System.lineSeparator() +
                "            InnovatorMartian(Double:-918291.348)" + System.lineSeparator() +
                "        InnovatorMartian(Double:-182.9)" + System.lineSeparator() +
                "    InnovatorMartian(Double:18.2419281)" + System.lineSeparator() +
                "        InnovatorMartian(Double:-7.0)" + System.lineSeparator(), tree.getFamilyReport());

        tree = new FamilyTree(c8);
        assertEquals("InnovatorMartian(Double:1.111)" + System.lineSeparator() +
                "    InnovatorMartian(Double:2.222)" + System.lineSeparator() +
                "    InnovatorMartian(Double:10230.12392)" + System.lineSeparator() +
                "        InnovatorMartian(Double:2.82873482E7)" + System.lineSeparator() +
                "            InnovatorMartian(Double:-918291.348)" + System.lineSeparator() +
                "        InnovatorMartian(Double:-182.9)" + System.lineSeparator() +
                "    InnovatorMartian(Double:18.2419281)" + System.lineSeparator() +
                "        InnovatorMartian(Double:-7.0)" + System.lineSeparator(), tree.getFamilyReport());

        tree = new FamilyTree(new ConservativeMartian<>(c2));
        assertEquals("ConservativeMartian(Double:1.111)" + System.lineSeparator() +
                "    ConservativeMartian(Double:2.222)" + System.lineSeparator() +
                "    ConservativeMartian(Double:10230.12392)" + System.lineSeparator() +
                "        ConservativeMartian(Double:2.82873482E7)" + System.lineSeparator() +
                "            ConservativeMartian(Double:-918291.348)" + System.lineSeparator() +
                "        ConservativeMartian(Double:-182.9)" + System.lineSeparator() +
                "    ConservativeMartian(Double:18.2419281)" + System.lineSeparator() +
                "        ConservativeMartian(Double:-7.0)" + System.lineSeparator(), tree.getFamilyReport());

        tree = new FamilyTree(new ConservativeMartian<>(c7));
        assertEquals("ConservativeMartian(Double:1.111)" + System.lineSeparator() +
                "    ConservativeMartian(Double:2.222)" + System.lineSeparator() +
                "    ConservativeMartian(Double:10230.12392)" + System.lineSeparator() +
                "        ConservativeMartian(Double:2.82873482E7)" + System.lineSeparator() +
                "            ConservativeMartian(Double:-918291.348)" + System.lineSeparator() +
                "        ConservativeMartian(Double:-182.9)" + System.lineSeparator() +
                "    ConservativeMartian(Double:18.2419281)" + System.lineSeparator() +
                "        ConservativeMartian(Double:-7.0)" + System.lineSeparator(), tree.getFamilyReport());
    }

    @Test
    void makeReport() {
        FamilyTree tree = new FamilyTree(c2);
        assertEquals("InnovatorMartian(Double:2.222)" + System.lineSeparator(), tree.makeReport(c2));

        assertEquals("InnovatorMartian(Double:10230.12392)" + System.lineSeparator() +
                "    InnovatorMartian(Double:2.82873482E7)" + System.lineSeparator() +
                "        InnovatorMartian(Double:-918291.348)" + System.lineSeparator() +
                "    InnovatorMartian(Double:-182.9)" + System.lineSeparator(), tree.makeReport(c3));

        assertEquals("ConservativeMartian(Double:1.111)" + System.lineSeparator() +
                        "    ConservativeMartian(Double:2.222)" + System.lineSeparator() +
                        "    ConservativeMartian(Double:10230.12392)" + System.lineSeparator() +
                        "        ConservativeMartian(Double:2.82873482E7)" + System.lineSeparator() +
                        "            ConservativeMartian(Double:-918291.348)" + System.lineSeparator() +
                        "        ConservativeMartian(Double:-182.9)" + System.lineSeparator() +
                        "    ConservativeMartian(Double:18.2419281)" + System.lineSeparator() +
                        "        ConservativeMartian(Double:-7.0)" + System.lineSeparator(),
                tree.makeReport(new ConservativeMartian<>(c1)));

        assertEquals("ConservativeMartian(Double:-918291.348)" + System.lineSeparator(),
                tree.makeReport(new ConservativeMartian<>(c8)));
    }

    @Test
    void constructReport() {
        FamilyTree tree = new FamilyTree(c5);
        assertEquals("InnovatorMartian(Double:-7.0)" + System.lineSeparator(),
                tree.constructReport(c7, "").toString());

        assertEquals("InnovatorMartian(Double:10230.12392)" + System.lineSeparator() +
                        "    InnovatorMartian(Double:2.82873482E7)" + System.lineSeparator() +
                        "        InnovatorMartian(Double:-918291.348)" + System.lineSeparator() +
                        "    InnovatorMartian(Double:-182.9)" + System.lineSeparator(),
                tree.constructReport(c3, "").toString());

        assertEquals("ConservativeMartian(Double:1.111)" + System.lineSeparator() +
                        "    ConservativeMartian(Double:2.222)" + System.lineSeparator() +
                        "    ConservativeMartian(Double:10230.12392)" + System.lineSeparator() +
                        "        ConservativeMartian(Double:2.82873482E7)" + System.lineSeparator() +
                        "            ConservativeMartian(Double:-918291.348)" + System.lineSeparator() +
                        "        ConservativeMartian(Double:-182.9)" + System.lineSeparator() +
                        "    ConservativeMartian(Double:18.2419281)" + System.lineSeparator() +
                        "        ConservativeMartian(Double:-7.0)" + System.lineSeparator(),
                tree.constructReport(new ConservativeMartian<>(c1), "").toString());

        assertEquals("ConservativeMartian(Double:-918291.348)" + System.lineSeparator(),
                tree.constructReport(new ConservativeMartian<>(c8), "").toString());

        assertThrows(Exception.class, () -> tree.constructReport(new ConservativeMartian<String>("abc".repeat(200),
                null, new ArrayList<>()), ""));

        assertThrows(Exception.class, () -> tree.constructReport(new ConservativeMartian<String>("a".repeat(257),
                null, new ArrayList<>()), ""));

        assertDoesNotThrow(() -> tree.constructReport(new InnovatorMartian<>("b".repeat(256),
                null, new ArrayList<>()), ""));
    }

    @Test
    void parseLine() {
        FamilyTree tree = new FamilyTree(c1);
        assertEquals(new InnovatorMartian("1", null, new ArrayList<>()).getValue(),
                tree.parseLine("InnovatorMartian(String:1)", null, new ArrayList<>()).getValue());

        ArrayList<Martian<Integer>> childs = new ArrayList<>();
        childs.add(new InnovatorMartian<>(89, null, new ArrayList<>()));
        assertEquals(new InnovatorMartian(167, null, childs).getValue(),
                tree.parseLine("InnovatorMartian(Integer:167)", null, childs).getValue());

        assertEquals(new InnovatorMartian(1.8727, null, new ArrayList<>()).getValue(),
                tree.parseLine("    InnovatorMartian(Double:1.8727)", null, new ArrayList<>()).getValue());

        assertEquals(new InnovatorMartian("abacaba", null, new ArrayList<>()).getValue(),
                tree.parseLine("InnovatorMartian(String:abacaba)", null, new ArrayList<>()).getValue());

        assertEquals(new InnovatorMartian(1876876876, null, new ArrayList<>()).getValue(),
                tree.parseLine("InnovatorMartian(Integer:1876876876)    ", null, new ArrayList<>()).getValue());

        assertEquals(new ConservativeMartian("1", null, new ArrayList<>()).getValue(),
                tree.parseLine("    ConservativeMartian(String:1)", null, new ArrayList<>()).getValue());

        assertEquals(new InnovatorMartian(463, null, childs).getValue(),
                tree.parseLine("        ConservativeMartian(Integer:463)    ", null, childs).getValue());

        assertEquals(new InnovatorMartian(-23.234, null, new ArrayList<>()).getValue(),
                tree.parseLine("ConservativeMartian(Double:-23.234)", null, new ArrayList<>()).getValue());

        assertEquals(new InnovatorMartian("hello world", null, new ArrayList<>()).getValue(),
                tree.parseLine("ConservativeMartian(String:hello world)", null, new ArrayList<>()).getValue());

        assertEquals(new InnovatorMartian(123456789, null, new ArrayList<>()).getValue(),
                tree.parseLine("ConservativeMartian(Integer:123456789)", null, new ArrayList<>()).getValue());

        assertThrows(Exception.class,
                () -> tree.parseLine("ConservativeMartin(Integer:123)", null, new ArrayList<>()).getValue());
        assertThrows(Exception.class,
                () -> tree.parseLine("(Integer:123)", null, new ArrayList<>()).getValue());
        assertThrows(Exception.class,
                () -> tree.parseLine("ConservativeMartian(:123)", null, new ArrayList<>()).getValue());
        assertThrows(Exception.class,
                () -> tree.parseLine("Conservative(Double:12.3)", null, new ArrayList<>()).getValue());
        assertThrows(Exception.class,
                () -> tree.parseLine("Innovator(Srting:321)", null, new ArrayList<>()).getValue());

        assertThrows(Exception.class, () -> tree.parseLine("ConservativeMartian(String:" + "abc".repeat(200) + ")",
                null, new ArrayList<>()).getValue());

        assertThrows(Exception.class, () -> tree.parseLine("ConservativeMartian(String:" + "a".repeat(257) + ")",
                null, new ArrayList<>()).getValue());

        assertDoesNotThrow(() -> tree.parseLine("InnovatorMartian(String:" + "b".repeat(256) + ")",
                null, new ArrayList<>()).getValue());
    }
}