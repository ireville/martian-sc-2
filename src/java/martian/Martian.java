package martian;

import java.util.ArrayList;
import java.util.List;

/* Некоторые пояснения к программе:
    1) Марисане были реализованы с помощью наследования. Почему так? Потому что у обоих типов (инноваторов и
    консерваторов) есть одинаково реализованные методы. Поэтому, чтобы не копировать одинаковые реализации и иметь
    возможность создавать коллекции, в которых могут присутствовать оба типа марсиан и был создан общий класс-предок.
    2) Некоторые методы в программе, у которых не прописан явно модификатор доступа, могли бы быть приватными. Они не
    сделаны таковыми для того, чтобы можно было протестировать их отдельно.
    3) Idea пишет варнинги на то, что некоторые if и for могут быть упрощены. Они могут, но полная их запись сделана
    для лучшей читаемости и понимая того, что это проверка условий.
 */

public abstract class Martian<T> {

    /**
     * @return Возвращает генетический код марсианина.
     */
    public abstract T getValue();

    /**
     * @return Возвращает родителя марсианина.
     */
    public abstract Martian<T> getParent();

    /**
     * Возвращает список всех детей марсианина.
     *
     * @return Список всех детей марсианина.
     */
    public abstract List<Martian<T>> getChildren();

    /**
     * Получает список всех наследников марсианина.
     *
     * @return Возвращает список всех наследников марсианина.
     */
    public List<Martian<T>> getDescendants() {
        ArrayList<Martian<T>> descendants = new ArrayList<>();
        for (Martian<T> child : getChildren()) {
            descendants.add(child);
            descendants.addAll(child.getDescendants());
        }

        Martian<T>[] descs = new Martian[descendants.size()];
        return List.of(descendants.toArray(descs));
    }

    /**
     * Проверяет, есть ли у данного марсианина ребёнок с заданным генетическим кодом.
     *
     * @param value Генетический код.
     * @return Истина, если есть, ложь - иначе.
     */
    public boolean hasChildWithValue(T value) {
        for (Martian<T> child : getChildren()) {
            if (value.equals(child.getValue())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Проверяет, есть ли у данного марсианина наследник с заданным генетическим кодом.
     *
     * @param value Генетический код.
     * @return Истина, если есть, ложь - иначе.
     */
    public boolean hasDescendantWithValue(T value) {
        for (Martian<T> child : getChildren()) {
            if (child.hasDescendantWithValue(value)) {
                return true;
            }
        }

        if (this.getValue().equals(value)) {
            return true;
        }

        return false;
    }

    /**
     * Проверяет, есть является ли данный марсианин наследником переданного.
     *
     * @param parent Значение предолагаемого родителя.
     * @return Истина, если является, ложь - иначе.
     */
    boolean isDescendantOf(Martian<T> parent) {
        List<Martian<T>> descendants = parent.getDescendants();
        for (Martian<T> desc : descendants) {
            if (desc.equals(this)) {
                return true;
            }
        }

        return false;
    }
}
