package martian;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class InnovatorMartianTest {
    static InnovatorMartian<Double> c1 = new InnovatorMartian<>(1.111, null, new ArrayList<>());
    static InnovatorMartian<Double> c2 = new InnovatorMartian<>(2.222, c1, new ArrayList<>());
    static InnovatorMartian<Double> c3 = new InnovatorMartian<>(10230.12392, c1, new ArrayList<>());
    static InnovatorMartian<Double> c4 = new InnovatorMartian<>(18.2419281, c1, new ArrayList<>());
    static InnovatorMartian<Double> c5 = new InnovatorMartian<>(28287348.2, c3, new ArrayList<>());
    static InnovatorMartian<Double> c6 = new InnovatorMartian<>(-182.90, c3, new ArrayList<>());
    static InnovatorMartian<Double> c7 = new InnovatorMartian<>(-7.0, c4, new ArrayList<>());
    static InnovatorMartian<Double> c8 = new InnovatorMartian<>(-918291.348, c5, new ArrayList<>());

    /*      c1
           / | \
         c2 c3 c4
            /\    \
          c5  c6  c7
          /
         c8
     */

    @BeforeEach
    void setUp() {
        ArrayList<Martian<Double>> childs = new ArrayList<>();
        childs.add(c8);
        c5 = new InnovatorMartian<>(28287348.2, null, childs);

        childs.clear();
        childs.add(c7);
        c4 = new InnovatorMartian<>(18.2419281, null, childs);

        childs.clear();
        childs.add(c5);
        childs.add(c6);
        c3 = new InnovatorMartian<>(10230.12392, null, childs);

        // OK - Должны перекопировать из коллекции, иначе плохо будет. -OK
        childs.clear();
        childs.add(c2);
        childs.add(c3);
        childs.add(c4);
        c1 = new InnovatorMartian<>(1.111, null, childs);

        c8.setParent(c5);
        c7.setParent(c4);
        c5.setParent(c3);
        c6.setParent(c3);
        c2.setParent(c1);
        c3.setParent(c1);
        c4.setParent(c1);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void setValue() {
        c8.setValue(1.0);
        assertEquals(1, c8.getValue());

        c7.setValue(134.2);
        assertEquals(134.2, c7.getValue());

        c5.setValue(987.9);
        assertEquals(987.9, c5.getValue());
        assertEquals(987.9, c8.getParent().getValue());

        c4.setValue(-5.5);
        assertEquals(-5.5, c4.getValue());
        assertEquals(-5.5, c7.getParent().getValue());

        c1.setValue(-3.3);
        assertEquals(-3.3, c2.getParent().getValue());
        assertEquals(-3.3, c4.getParent().getValue());
    }

    @Test
    void setParent() {
        assertThrows(Exception.class, () -> new InnovatorMartian<Double>(1.2, new ConservativeMartian<>(c1),
                new ArrayList<>()));
        assertThrows(Exception.class, () -> new InnovatorMartian<Double>(2.3, new ConservativeMartian<>(c8),
                new ArrayList<>()));

        c8.setParent(c4);
        assertEquals(c4, c8.getParent());
        assertFalse(c5.hasChildWithValue(-918291.348));
        assertEquals(0, c5.getChildren().size());

        assertFalse(c4.setParent(c7));
        assertEquals(c4, c7.getParent());
        assertEquals(c1, c4.getParent());

        setUp();

        assertTrue(c2.setParent(c5));
        assertEquals(c5, c2.getParent());
        assertEquals(2, c5.getChildren().size());

        assertFalse(c3.setParent(c5));
        assertFalse(c1.setParent(c3));
        assertFalse(c1.setParent(c2));

        assertThrows(Exception.class,
                () -> new InnovatorMartian<>(1.2, new ConservativeMartian<>(c2), new ArrayList<>()));
        assertThrows(Exception.class,
                () -> new InnovatorMartian<>(1.2, new ConservativeMartian<>(c3), new ArrayList<>()));
        assertThrows(Exception.class,
                () -> new InnovatorMartian<>(1.2, new ConservativeMartian<>(c1), new ArrayList<>()));
        assertThrows(Exception.class,
                () -> new InnovatorMartian<>(1.2,
                        new ConservativeMartian<Double>(1.1, null, new ArrayList<>()), new ArrayList<>()));
    }

    @Test
    void checkChildForThis() {
        assertTrue(c1.checkChildForThis(c8));
        assertTrue(c2.checkChildForThis(c7));
        assertTrue(c3.checkChildForThis(c6));
        assertTrue(c3.checkChildForThis(c2));
        assertTrue(c1.checkChildForThis(c2));
        assertFalse(c8.checkChildForThis(c1));
        assertFalse(c5.checkChildForThis(c3));
        assertTrue(c7.checkChildForThis(c2));
    }

    @Test
    void checkParentForThis() {
        assertFalse(c1.checkParentForThis(c8));
        assertTrue(c2.checkParentForThis(c7));
        assertFalse(c3.checkParentForThis(c6));
        assertTrue(c3.checkParentForThis(c2));
        assertFalse(c1.checkParentForThis(c2));
        assertTrue(c8.checkParentForThis(c1));
        assertTrue(c5.checkParentForThis(c3));
        assertTrue(c7.checkParentForThis(c2));
    }

    @Test
    void setChildren() {
        assertTrue(c1.setChildren(c2.getChildren()));
        assertEquals(0, c1.getChildren().size());

        assertTrue(c1.setChildren(c3.getChildren()));
        assertEquals(2, c1.getChildren().size());
        assertEquals(3, c1.getDescendants().size());
        assertEquals(c5, c8.getParent());
        assertEquals(c1, c8.getParent().getParent());

        ArrayList<Martian<Double>> ch = new ArrayList<>();
        ch.add(new ConservativeMartian<>(-9.8, null, new ArrayList<>()));

        assertFalse(c1.setChildren(ch));

        ArrayList<Martian<Double>> arr = new ArrayList<Martian<Double>>();
        arr.add(new ConservativeMartian<>(c2));
        assertThrows(Exception.class,
                () -> new InnovatorMartian<>(1.2, null, arr));

        arr.clear();
        arr.add(new ConservativeMartian<>(c5));
        assertThrows(Exception.class,
                () -> new InnovatorMartian<>(1.8, null, arr));

        setUp();

        assertTrue(c1.setChildren(null));
        assertEquals(0, c1.getChildren().size());
        assertNull(c2.getParent());
        assertNull(c3.getParent());
        assertEquals(2, c3.getChildren().size());
        assertNull(c4.getParent());

        assertTrue(c8.setChildren(null));
        assertTrue(c5.setChildren(null));
        assertEquals(0, c5.getChildren().size());
        assertNull(c8.getParent());
    }

    @Test
    void addChild() {
        InnovatorMartian<Double> newM = new InnovatorMartian<>(1.9, null, new ArrayList<>());
        assertTrue(c1.addChild(newM));
        assertEquals(4, c1.getChildren().size());
        assertEquals(c1, newM.getParent());

        assertTrue(newM.addChild(c3));
        assertEquals(3, c1.getChildren().size());
        assertEquals(newM, c6.getParent().getParent());

        setUp();

        assertTrue(c3.addChild(newM));
        assertEquals(3, c3.getChildren().size());
        assertEquals(c3, newM.getParent());
        assertEquals(3, c1.getChildren().size());

        assertFalse(c1.addChild(new ConservativeMartian<>(c2)));
        assertFalse(c1.addChild(new ConservativeMartian<>(c8)));
        assertFalse(c3.addChild(c1));

        assertFalse(c1.addChild(null));
        assertFalse(c8.addChild(null));
        assertFalse(c5.addChild(null));
    }

    @Test
    void deleteChild() {
        c5.deleteChild(c8);
        assertEquals(0, c5.getChildren().size());
        assertNull(c8.getParent());

        c1.deleteChild(c4);
        assertEquals(2, c1.getChildren().size());
        assertEquals(c1, c3.getParent());
        assertNull(c4.getParent());
        assertEquals(c4, c7.getParent());
        assertNull(c7.getParent().getParent());

        c1.deleteChild(c3);
        assertEquals(1, c1.getChildren().size());
        assertNull(c3.getParent());
        assertEquals(c1, c2.getParent());
        assertEquals(c3, c5.getParent());
        assertNull(c5.getParent().getParent());

        assertFalse(c1.hasDescendantWithValue(-7.0));
        assertTrue(c4.hasDescendantWithValue(-7.0));
        assertFalse(c1.hasDescendantWithValue(-918291.348));

        assertFalse(c1.deleteChild(new ConservativeMartian<>(c2)));
        assertFalse(c1.deleteChild(new ConservativeMartian<>(c8)));
    }
}