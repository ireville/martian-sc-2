package martian;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ConservativeMartianTest {

    static InnovatorMartian<String> c1 = new InnovatorMartian<>("c1", null, new ArrayList<>());
    static InnovatorMartian<String> c2 = new InnovatorMartian<>("c2", c1, new ArrayList<>());
    static InnovatorMartian<String> c3 = new InnovatorMartian<>("c3", c1, new ArrayList<>());
    static InnovatorMartian<String> c4 = new InnovatorMartian<>("c4", c1, new ArrayList<>());
    static InnovatorMartian<String> c5 = new InnovatorMartian<>("c5", c3, new ArrayList<>());
    static InnovatorMartian<String> c6 = new InnovatorMartian<>("c6", c3, new ArrayList<>());
    static InnovatorMartian<String> c7 = new InnovatorMartian<>("c7", c4, new ArrayList<>());
    static InnovatorMartian<String> c8 = new InnovatorMartian<>("c8", c5, new ArrayList<>());
    static ConservativeMartian<String> root = new ConservativeMartian<>("", null, new ArrayList<>());

    /*      c1
           / | \
         c2 c3 c4
            /\    \
          c5  c6  c7
          /
         c8
     */

    @BeforeEach
    void setUp() {
        ArrayList<Martian<String>> childs = new ArrayList<>();
        childs.add(c8);
        c5 = new InnovatorMartian<>("c5", null, childs);

        childs.clear();
        childs.add(c7);
        c4 = new InnovatorMartian<>("c4", null, childs);

        childs.clear();
        childs.add(c5);
        childs.add(c6);
        c3 = new InnovatorMartian<>("c3", null, childs);

        // OK - Должны перекопировать из коллекции, иначе плохо будет. -OK
        childs.clear();
        childs.add(c2);
        childs.add(c3);
        childs.add(c4);
        c1 = new InnovatorMartian<>("c1", null, childs);

        c8.setParent(c5);
        c7.setParent(c4);
        c5.setParent(c3);
        c6.setParent(c3);
        c2.setParent(c1);
        c3.setParent(c1);
        c4.setParent(c1);
    }

    @Test
    void getValue() {
        root = root.getRightChild(c8);
        assertEquals(c5.getValue(), root.getParent().getValue());
        assertEquals(c3.getValue(), root.getParent().getParent().getValue());
        assertEquals(c1.getValue(), root.getParent().getParent().getParent().getValue());

        root = root.getRightChild(c7);
        assertEquals(c4.getValue(), root.getParent().getValue());
        assertEquals(c1.getValue(), root.getParent().getParent().getValue());

        root = root.getRightChild(c2);
        assertEquals(c1.getValue(), root.getParent().getValue());


        root = root.buildConservativeFromInnovator(c1, null);
        assertEquals(c5.getValue(), root.getChildren().get(1).getChildren().get(0).getValue());
        assertEquals(c6.getValue(), root.getChildren().get(1).getChildren().get(1).getValue());
        assertEquals(c7.getValue(), root.getChildren().get(2).getChildren().get(0).getValue());
    }

    @Test
    void getParent() {
        root = root.getRightChild(c8);
        assertEquals(c5.getValue(), root.getParent().getValue());
        assertEquals(c3.getValue(), root.getParent().getParent().getValue());
        assertEquals(c1.getValue(), root.getParent().getParent().getParent().getValue());

        root = root.getRightChild(c7);
        assertEquals(c4.getValue(), root.getParent().getValue());
        assertEquals(c1.getValue(), root.getParent().getParent().getValue());

        root = root.getRightChild(c2);
        assertEquals(c1.getValue(), root.getParent().getValue());
    }

    @Test
    void getChildren() {
        root = root.buildConservativeFromInnovator(c1, null);
        assertEquals(3, root.getChildren().size());
        assertEquals("c2", root.getChildren().get(0).getValue());
        assertEquals("c3", root.getChildren().get(1).getValue());
        assertEquals("c4", root.getChildren().get(2).getValue());
        assertEquals(c2.getChildren().size(), root.getChildren().get(0).getChildren().size());
        assertEquals(c3.getChildren().size(), root.getChildren().get(1).getChildren().size());
        assertEquals(c4.getChildren().size(), root.getChildren().get(2).getChildren().size());
        assertEquals(c5.getValue(), root.getChildren().get(1).getChildren().get(0).getValue());
        assertEquals(c6.getValue(), root.getChildren().get(1).getChildren().get(1).getValue());
        assertEquals(c7.getValue(), root.getChildren().get(2).getChildren().get(0).getValue());
        assertEquals(c8.getValue(), root.getChildren().get(1).getChildren().get(0).getChildren().get(0).getValue());

        assertThrows(Exception.class, () -> c1.getChildren().set(0, new ConservativeMartian<>(c1)));
        assertThrows(Exception.class, () -> c8.getChildren().set(0, new ConservativeMartian<>(c2)));
        assertThrows(Exception.class, () -> c1.getChildren().set(0, c5));
        assertThrows(Exception.class, () -> c8.getChildren().set(0, c4));
    }

    @Test
    void getChildIndex() {
        ConservativeMartian<String> cons = new ConservativeMartian<>(c3);
        assertEquals(1, cons.getChildIndex(c3));
        assertEquals(0, cons.getChildIndex(c2));
        assertEquals(2, cons.getChildIndex(c4));
        assertEquals(0, cons.getChildIndex(c5));
        assertEquals(1, cons.getChildIndex(c6));
        assertEquals(0, cons.getChildIndex(c8));
        assertEquals(-1, cons.getChildIndex(c1));
        assertEquals(0, cons.getChildIndex(c8));
        assertEquals(0, cons.getChildIndex(c8));
    }

    @Test
    void getRightChild() {
        ConservativeMartian<String> cons = new ConservativeMartian<>("", null, new ArrayList<>());

        cons = cons.getRightChild(c3);
        assertEquals(c1.getValue(), cons.getParent().getValue());
        assertEquals(c1.getChildren().get(2).getValue(), cons.getParent().getChildren().get(2).getValue());
        assertEquals(c3.getChildren().get(0).getValue(), cons.getChildren().get(0).getValue());
        assertEquals(c5.getValue(), cons.getChildren().get(0).getValue());
        assertEquals(c6.getValue(), cons.getChildren().get(1).getValue());
        assertEquals(c8.getValue(), cons.getChildren().get(0).getChildren().get(0).getValue());

        cons = cons.getRightChild(c7);
        assertEquals(c7.getValue(), cons.getValue());
        assertEquals(c4.getValue(), cons.getParent().getValue());
        assertEquals(c1.getValue(), cons.getParent().getParent().getValue());
        assertEquals(c3.getValue(), cons.getParent().getParent().getChildren().get(1).getValue());
        assertEquals(c6.getValue(), cons.getParent().getParent().getChildren().get(1).getChildren().get(1).getValue());
    }

    @Test
    void buildConservativeFromInnovator() {
        root = root.buildConservativeFromInnovator(c1, null);

        assertEquals(c1.getValue(), root.getValue());
        assertEquals(c1.getChildren().size(), root.getChildren().size());
        assertEquals(3, root.getChildren().size());
        assertEquals(c2.getValue(), root.getChildren().get(0).getValue());
        assertEquals(c3.getValue(), root.getChildren().get(1).getValue());
        assertEquals(c4.getValue(), root.getChildren().get(2).getValue());
        assertEquals(c2.getChildren().size(), root.getChildren().get(0).getChildren().size());
        assertEquals(c3.getChildren().size(), root.getChildren().get(1).getChildren().size());
        assertEquals(c4.getChildren().size(), root.getChildren().get(2).getChildren().size());
        assertEquals(c5.getValue(), root.getChildren().get(1).getChildren().get(0).getValue());
        assertEquals(c6.getValue(), root.getChildren().get(1).getChildren().get(1).getValue());
        assertEquals(c7.getValue(), root.getChildren().get(2).getChildren().get(0).getValue());
        assertEquals(c8.getValue(), root.getChildren().get(1).getChildren().get(0).getChildren().get(0).getValue());
    }
}