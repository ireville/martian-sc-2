package martian;

import java.util.ArrayList;

public class FamilyTree {
    private Object root;
    private String familyReport;

    /**
     * Создаёт семейное дерево переданного марсианина.
     *
     * @param martian Марсианин, для которого строится дерево.
     * @param <T>     Генетический код марсианина.
     */
    public <T> FamilyTree(Martian<T> martian) {
        if (martian == null) {
            root = null;
            familyReport = "No family tree for null martian. ";
            return;
        }

        while (martian.getParent() != null) {
            martian = martian.getParent();
        }

        root = martian;
        familyReport = makeReport(martian);
    }

    /**
     * Создаёт семейное дерево по переданному отчёту.
     *
     * @param report Отчёт.
     */
    public FamilyTree(String report) {
        root = buildTree(report);
        familyReport = report;
    }

    /**
     * Возвращает корень семейного дерева.
     *
     * @return Корень семейного дерева.
     */
    public Object getRoot() {
        return root;
    }

    /**
     * Возвращает отчёт по семейному дереву.
     *
     * @return Отчёт по семейному дереву.
     */
    public String getFamilyReport() {
        return familyReport;
    }

    /**
     * Создаёт отчёт для марсианина.
     *
     * @param martian Марсианин, для которого создаётся отчёт.
     * @param <T>     Генетический код марсианина.
     * @return Отчёт.
     */
    protected <T> String makeReport(Martian<T> martian) {
        return constructReport(martian, "").toString();
    }

    /**
     * Рекурсивно построчно конструирует отчёт для марсианина.
     *
     * @param martian Марсианин, для которого создаётся отчёт.
     * @param tab     Текущий отступ в строке отчёта.
     * @param <T>     Генетический код марсианина.
     * @return Отчёт.
     */
    <T> StringBuilder constructReport(Martian<T> martian, String tab) {
        if (martian.getValue().toString().length() > 256) {
            throw new IllegalArgumentException("Too long string value length. ");
        }

        StringBuilder sb = new StringBuilder(tab + martian.getClass().getSimpleName() + "(" +
                martian.getValue().getClass().getSimpleName() + ":" + martian.getValue().toString() + ")"
                + System.lineSeparator());
        for (Martian<T> child : martian.getChildren()) {
            sb.append(constructReport(child, tab + "    "));
        }

        return sb;
    }

    /**
     * Строит семейное дерево по переданному отчёту.
     *
     * @param report Отчёт.
     * @return Корень построенного дерева.
     */
    Object buildTree(String report) {
        String[] lines = report.split("\\r?\\n");
        return recursiveBuild(0, lines, "", null);
    }

    /**
     * Рекурсивно проходится по отчёту, строя дерево.
     *
     * @param strIndex    Номер текущей обрабатываемой строки.
     * @param lines       Список строк отчёта.
     * @param requiredTab Ожидаемый отступ в стркое для создаваемого марсианина.
     * @param parent      Родитель для создаваемого объекта марсианина
     * @param <T>         генетический код марсианина.
     * @return Корень дерева.
     */
    private <T> Martian<T> recursiveBuild(int strIndex, String[] lines, String requiredTab, Martian<T> parent) {
        ArrayList<Martian<T>> childs = new ArrayList<>();
        Martian<T> current = parseLine(lines[strIndex], parent, childs);

        for (int i = strIndex + 1; i < lines.length; i++) {
            if (lines[i].startsWith(requiredTab + "    ")) {
                if (lines[i].substring(requiredTab.length() + 4).startsWith("I") ||
                        lines[i].substring(requiredTab.length() + 4).startsWith("C")) {
                    if (current instanceof ConservativeMartian) {
                        childs.add(recursiveBuild(i, lines, requiredTab + "    ", current));
                    } else {
                        ((InnovatorMartian<T>) current).addChild(recursiveBuild(i, lines,
                                requiredTab + "    ", current));
                    }
                }
            } else {
                break;
            }
        }

        return current;
    }

    /**
     * Преобразует строку отчёта в марсианина.
     *
     * @param info   Текущая строка для обработки
     * @param parent Родитель для создаваемого марсианина.
     * @param childs Дети для создаваемого марсианина.
     * @param <T>    Генетический код марсианина.
     * @return Построенный марсианин.
     */
    <T> Martian<T> parseLine(String info, Martian<T> parent, ArrayList<Martian<T>> childs) {
        info = info.trim();

        if (!info.matches("^.*Martian\\(.+?:.+\\)$")) {
            throw new IllegalArgumentException("Incorrect format of the report string. ");
        }

        boolean martianType = false;
        String valueType = info.substring("ConservativeMartian(".length());

        if (info.startsWith("InnovatorMartian")) {
            martianType = true;
            valueType = info.substring("InnovatorMartian(".length());
        }

        T value;
        if (valueType.startsWith("Double")) {
            String valueStr = valueType.substring("Double:".length());
            valueStr = valueStr.substring(0, valueStr.length() - 1);

            double valueDouble = Double.parseDouble(valueStr);

            value = (T) (Object) valueDouble;
        } else if (valueType.startsWith("Integer")) {
            String valueStr = valueType.substring("Integer:".length());
            valueStr = valueStr.substring(0, valueStr.length() - 1);
            int valueInteger = Integer.parseInt(valueStr);

            value = (T) (Object) valueInteger;
        } else {
            String valueStr = valueType.substring("String:".length());
            valueStr = valueStr.substring(0, valueStr.length() - 1);

            if (valueStr.length() > 256) {
                throw new IllegalArgumentException("Too long string value length. ");
            }

            value = (T) valueStr;
        }

        if (martianType) {
            return new InnovatorMartian<>(value, parent, childs);
        } else {
            return new ConservativeMartian<>(value, parent, childs);
        }
    }
}