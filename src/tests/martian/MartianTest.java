package martian;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.regex.MatchResult;

import static org.junit.jupiter.api.Assertions.*;

class MartianTest {
    static Martian<Integer> m1 = new ConservativeMartian<>(1, null, new ArrayList<>());
    static Martian<Integer> m2 = new ConservativeMartian<>(2, null, new ArrayList<>());
    static Martian<Integer> m3 = new ConservativeMartian<>(1023012392, null, new ArrayList<>());
    static Martian<Double> m4 = new ConservativeMartian<>(182419281.8, null, new ArrayList<>());
    static Martian<String> m5 = new ConservativeMartian<>("hello", null, new ArrayList<>());

    static Martian<Double> m = new ConservativeMartian<>(1.1, null, new ArrayList<>());

    static Martian<Double> m6 = new ConservativeMartian<>(182419281.8, m, new ArrayList<>());
    static Martian<Double> m7 = new ConservativeMartian<>(198347.1236, m, new ArrayList<>());
    static Martian<Double> m8 = new ConservativeMartian<>(7.7, m, new ArrayList<>());

    static InnovatorMartian<Integer> c1 = new InnovatorMartian<>(1, null, new ArrayList<>());
    static InnovatorMartian<Integer> c2 = new InnovatorMartian<>(2, c1, new ArrayList<>());
    static InnovatorMartian<Integer> c3 = new InnovatorMartian<>(1023012392, c1, new ArrayList<>());
    static InnovatorMartian<Integer> c4 = new InnovatorMartian<>(182419281, c1, new ArrayList<>());
    static InnovatorMartian<Integer> c5 = new InnovatorMartian<>(282873482, c3, new ArrayList<>());
    static InnovatorMartian<Integer> c6 = new InnovatorMartian<>(-182, c3, new ArrayList<>());
    static InnovatorMartian<Integer> c7 = new InnovatorMartian<>(-7, c4, new ArrayList<>());
    static InnovatorMartian<Integer> c8 = new InnovatorMartian<>(-918291, c5, new ArrayList<>());

    /*      c1
           / | \
         c2 c3 c4
            /\    \
          c5  c6  c7
          /
         c8
     */

    @BeforeEach
    void setUp() {

        ArrayList<Martian<Integer>> childs = new ArrayList<>();
        childs.add(c8);
        c5 = new InnovatorMartian<>(282873482, null, childs);

        childs.clear(); childs.add(c7);
        c4 = new InnovatorMartian<Integer>(182419281, null, childs);

        childs.clear(); childs.add(c5); childs.add(c6);
        c3 = new InnovatorMartian<>(1023012392, null, childs);

        // OK - Должны перекопировать из коллекции, иначе плохо будет. -OK
        childs.clear(); childs.add(c2); childs.add(c3); childs.add(c4);
        c1 = new InnovatorMartian<>(1, null, childs);

        c8.setParent(c5); c7.setParent(c4); c5.setParent(c3); c6.setParent(c3);
        c2.setParent(c1); c3.setParent(c1); c4.setParent(c1);
    }

    @AfterEach
    void tearDown() {}

    @Test
    void getValue() {
        assertEquals(1, m1.getValue());
        assertEquals(2, m2.getValue());
        assertEquals(1023012392, m3.getValue());
        assertEquals(182419281.8, m4.getValue());
        assertEquals("hello", m5.getValue());

        assertEquals(2, c1.getChildren().get(0).getValue());
        assertEquals(2, c1.getChildren().get(0).getValue());
        assertEquals(182419281, c1.getChildren().get(2).getValue());
        assertEquals(1023012392, c3.getValue());

        var cnt = c3.getChildren().size();
        var a = (c3.getChildren()).get(1);

        assertEquals( -182, c3.getChildren().get(1).getValue());
        assertEquals( 282873482, c1.getChildren().get(1).getChildren().get(0).getValue());
        assertEquals( -918291, c1.getChildren().get(1).getChildren().get(0).getChildren().get(0).getValue());
    }

    @Test
    void getParent() {
        assertNull(m1.getParent());
        assertNull(m5.getParent());

        //assertEquals(m, m6.getParent());
        assertEquals(m, m7.getParent());
        assertEquals(m, m8.getParent());


        assertEquals(c1, c2.getParent());
        assertEquals(c1, c3.getParent());
        assertEquals(c1, c4.getParent());
        assertEquals(c3, c5.getParent());
        assertEquals(c3, c6.getParent());
        assertEquals(c4, c7.getParent());
        assertEquals(c5, c8.getParent());
    }

    @Test
    void getChildren() {
        assertEquals(new ArrayList<Martian<Integer>>(),  m1.getChildren());
        assertEquals(new ArrayList<Martian<Double>>(),  m4.getChildren());
        assertEquals(new ArrayList<Martian<String>>(),  m5.getChildren());

        ArrayList<Martian<Double>> children = new ArrayList<Martian<Double>>();
        children.add(m4);
        children.add(m6);
        children.add(m7);
        children.add(m8);

        Martian<Double> mm = new ConservativeMartian<Double>(132.0, null, children);

        assertEquals(children, mm.getChildren());

        assertEquals(c2, c1.getChildren().get(0));
        assertEquals(c3, c1.getChildren().get(1));
        assertEquals(c4, c1.getChildren().get(2));
        assertEquals(c5, c3.getChildren().get(0));
        assertEquals(c6, c3.getChildren().get(1));
        assertEquals(c7, c4.getChildren().get(0));
        assertEquals(c8, c5.getChildren().get(0));

        ArrayList<Martian<Integer>> childs = new ArrayList<>();
        childs.add(c2); childs.add(c3); childs.add(c4);
        assertEquals(childs, c1.getChildren());

        childs.clear();
        assertEquals(childs, c8.getChildren());

        childs.add(c8);
        assertEquals(childs, c5.getChildren());

        childs.clear(); childs.add(c5); childs.add(c6);
        assertEquals(childs, c3.getChildren());

        assertThrows(Exception.class, () -> c1.getChildren().set(0, new ConservativeMartian<>(c1)));
        assertThrows(Exception.class, () -> c8.getChildren().set(0, new ConservativeMartian<>(c2)));
        assertThrows(Exception.class, () -> c1.getChildren().set(0, c5));
        assertThrows(Exception.class, () -> c8.getChildren().set(0, c4));
    }

    @Test
    void getDescendants() {
        ArrayList<Martian<Integer>> descs = new ArrayList<>();
        descs.add(c2); descs.add(c3); descs.add(c5); descs.add(c8); descs.add(c6); descs.add(c4); descs.add(c7);
        assertEquals(descs, c1.getDescendants());

        descs.clear();
        assertEquals(descs, c8.getDescendants());

        descs.add(c8);
        assertEquals(descs, c5.getDescendants());

        descs.clear(); descs.add(c5); descs.add(c8); descs.add(c6);
        assertEquals(descs, c3.getDescendants());

        descs.clear(); descs.add(c7);
        assertEquals(descs, c4.getDescendants());
    }

    @Test
    void isDescendantOf() {
        assertTrue(c2.isDescendantOf(c1));
        assertTrue(c7.isDescendantOf(c1));
        assertTrue(c7.isDescendantOf(c4));
        assertTrue(c5.isDescendantOf(c3));
        assertFalse(c1.isDescendantOf(c2));
        assertFalse(c1.isDescendantOf(c8));
        assertFalse(c3.isDescendantOf(c5));
        assertFalse(c3.isDescendantOf(c3));
    }

    @Test
    void hasChildWithValue() {
        ArrayList<Martian<String>> children = new ArrayList<>();
        children.add(m5);

        Martian<String> mm = new ConservativeMartian<>("bye", null, children);
        assertTrue(mm.hasChildWithValue("hello"));

        ArrayList<Martian<Double>> childs = new ArrayList<>();
        childs.add(m8);

        Martian<Double> mm2 = new ConservativeMartian<>(1.1, null, childs);
        assertTrue(mm2.hasChildWithValue(7.7));

        assertTrue(c1.hasChildWithValue(182419281));
        assertTrue(c5.hasChildWithValue(-918291));
        assertTrue(c3.hasChildWithValue(-182));
        assertFalse(c1.hasChildWithValue(282873482));
        assertTrue(c1.hasChildWithValue(1023012392));
    }

    @Test
    void hasDescendantWithValue() {
        ArrayList<Martian<String>> children = new ArrayList<>();
        children.add(m5);

        Martian<String> mm = new ConservativeMartian<String>("bye", null, children);
        assertTrue(mm.hasDescendantWithValue("hello"));

        ArrayList<Martian<Double>> childs = new ArrayList<>();
        childs.add(m8);

        Martian<Double> mm2 = new ConservativeMartian<Double>(1.1, null, childs);
        assertTrue(mm2.hasDescendantWithValue(7.7));

        assertTrue(c1.hasDescendantWithValue(182419281));
        assertTrue(c3.hasDescendantWithValue(-918291));
        assertTrue(c5.hasDescendantWithValue(-918291));
        assertTrue(c1.hasDescendantWithValue(-918291));
        assertTrue(c3.hasDescendantWithValue(-182));
        assertTrue(c1.hasDescendantWithValue(-182));
        assertTrue(c1.hasDescendantWithValue(1023012392));
    }
}